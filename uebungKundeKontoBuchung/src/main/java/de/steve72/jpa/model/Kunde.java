package de.steve72.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Kunde {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer kundeId;

    @OneToMany(mappedBy = "kunde", cascade = CascadeType.PERSIST)
    private Set<Konto> konten;

    @Column(length = 30)
    private String first_name;

    @Column(length = 40)
    private String last_name;

    //region Konstruktoren
    public Kunde() {

        this.konten = new HashSet<>();
    }

    public Kunde(String first_name, String last_name) {

        this();
        this.first_name = first_name;
        this.last_name = last_name;
    }
    //endregion

    //region Getter/Setter
    public Integer getKundeId() {
        return kundeId;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Set<Konto> getKonten() {
        return konten;
    }

    public void setKonten(Set<Konto> konten) {
        this.konten = konten;
    }

    //endregion
}
