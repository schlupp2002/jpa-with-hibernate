drop database if exists jpawh;

create database jpawh;

source tbl_kundeUUID.sql;
source tbl_kundeAUTO.sql;
source tbl_kundeTABLE.sql;
source tbl_kundeIDENTITY.sql;

use jpawh;

select "UUID" as Info;
describe KundeUUID;

select "AUTO" as Info;
describe KundeAUTO;

select "TABLE" as Info;
describe KundeTABLE;

select "IDENTITY" as Info;
describe KundeIDENTITY;
