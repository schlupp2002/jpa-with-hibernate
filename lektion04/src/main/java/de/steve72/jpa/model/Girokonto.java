package de.steve72.jpa.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@DiscriminatorValue("Girokonto")
public class Girokonto extends AKonto {

    private BigDecimal dispo;

    public Girokonto() {

        super("noname");
        this.dispo=new BigDecimal(0);
    }

    public Girokonto(String bankname) {
        super(bankname);
        this.dispo = new BigDecimal(0);
    }

    public Girokonto(String bankname, BigDecimal dispo) {

        super(bankname);
        this.dispo = dispo;
    }
}
