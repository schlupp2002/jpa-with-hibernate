package de.steve72.jpa.model;

import javax.persistence.PrePersist;


/**
 * Created by bauer on 15.06.16.
 */
public class KundeValidierer {

    @PrePersist
    public void validiere(Kunde kunde) {

        if (kunde.getVorname() == null) {

            throw new IllegalArgumentException("Vorname darf nicht 'null' sein");
        }

        if (kunde.getVorname().length() < 3 || kunde.getVorname().length() > 40) {
            throw new IllegalArgumentException("Vorname muss zwischen 3..40 Zeichen lang sein");
        }
    }
}
