package de.steve72.jpa;

import de.steve72.jpa.model.Buchung;
import de.steve72.jpa.model.Girokonto;
import de.steve72.jpa.model.Konto;
import de.steve72.jpa.model.Kunde;
import de.steve72.jpa.model.Sparkonto;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.util.List;


public class MainApp {

    public static void main(String... args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlPU");
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        // einen Kunden anlegen
        Kunde steffen = new Kunde("Steffen", "Bauer");
        em.persist(steffen);

        // ein Girokonto für Steffen
        Konto giro = new Girokonto("Sal. Bauersheimer", new BigDecimal(10000.00f));

        steffen.getKonten().add(giro);
        giro.setKunde(steffen);

        // ein Sparkonto für Steffen
        Konto spar = new Sparkonto("Sal. Bauersheimer", new BigDecimal(1.2f));

        steffen.getKonten().add(spar);
        spar.setKunde(steffen);

        giro.einzahlen(new BigDecimal(10000.00f), "Panama-Dokumente");
        giro.einzahlen(new BigDecimal(20000.00f), "Wahlkampfspende Donald Duck");
        giro.einzahlen(new BigDecimal(20000.00f), "Urlaubsgeld");
        em.flush();

        spar.einzahlen(new BigDecimal(1000.00f), "Sparstrumpf füllen");
        spar.einzahlen(new BigDecimal(1000.00f), "Sparstrumpf füllen");
        spar.einzahlen(new BigDecimal(1000.00f), "Sparstrumpf füllen");
        em.flush();

        giro.auszahlen(new BigDecimal(2000.00f), "Bestechungsgeld");

        em.getTransaction().commit();

        em.clear();
        emf.close();
    }
}
