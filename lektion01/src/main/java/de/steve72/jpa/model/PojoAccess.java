package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Transient;

@Entity
public class PojoAccess {

	private long id;

	private String vorname;
	private String nachname;

	public PojoAccess() {

		this("Jane", "Doe");
	}

	public PojoAccess(String vorname, String nachname) {

		this.vorname = vorname;
		this.nachname = nachname;
	}

	// wir konfigurieren für die gesamte Entity "property access",
	// weil die @Id-Annotation an der Getter-Methode erfolgt
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public long getId() {

		return this.id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getVorname() {

		return this.vorname;
	}

	public void setVorname(String vorname) {

		this.vorname = vorname;
	}

	
	public String getNachname() {

		return this.nachname;
	}

	public void setNachname(String nachname) {

		this.nachname = nachname;
	}

	// aufpassen mit get-/set-Methoden
	@Transient
	public String getVollerName() {

		return String.format("%s, %s", this.nachname, this.vorname);
	}

	public void setVollerName(String beide) {

		String [] tmp = beide.split(" ");

		this.vorname=tmp[0];
		this.nachname=tmp[1];
	}
}







