package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


/**
 * Created by bauer on 15.06.16.
 */
@Entity
public class KundeBi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String vorname;
    private String nachname;

    @OneToOne
    @JoinColumn(name="adressebi", unique = true)
    private AdresseBi adressebi;

    public KundeBi() {
    }

    public KundeBi(String vorname, String nachname) {
        this.vorname = vorname;
        this.nachname = nachname;
    }

    @Override
    public String toString() {

        return String.format("[%d]: %s, %s wohnt in %s", id, vorname, nachname, adressebi);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public AdresseBi getAdressebi() {
        return adressebi;
    }

    public void setAdressebi(AdresseBi adressebi) {
        this.adressebi = adressebi;
    }
}
