package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(KundeId.class)
public class KundeIDCLASS {

  @Id
  private String abt;

  @Id
  private int num;

  private String vorname;
  private String nachname;

  public KundeIDCLASS() {
  
  }

  public KundeIDCLASS(String abt, int num, String vorname, String nachname) {

    this.abt=abt;
    this.num=num;
  
    this.vorname=vorname;
    this.nachname=nachname;
  }

  @Override
  public String toString() {

    return String.format("%s: [%s, %d] %s, %s", this.getClass().getSimpleName(), this.abt, this.num,   this.nachname, this.vorname);
  }
}
