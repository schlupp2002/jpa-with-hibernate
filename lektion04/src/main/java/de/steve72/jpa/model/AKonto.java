package de.steve72.jpa.model;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
      name = "Art",
      discriminatorType = DiscriminatorType.STRING)
public abstract class AKonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer kontonummer;

    protected String bankname;

    //region Konstruktoren
    public AKonto() {
    }

    public AKonto(String bankname) {
        this.bankname = bankname;
    }
    //endregion

    @Override
    public String toString() {

        return String.format("[%s]: [%2d] %s", this.getClass().getSimpleName(), kontonummer, bankname);
    }
}
