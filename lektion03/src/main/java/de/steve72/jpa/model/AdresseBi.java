package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


/**
 * Created by bauer on 15.06.16.
 */
@Entity
public class AdresseBi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(mappedBy = "adressebi")
    private KundeBi kundebi;

    private String strasse;
    private String hausnummer;
    private String plz;
    private String ort;

    public AdresseBi() {
    }

    public AdresseBi(String strasse, String hausnummer, String plz, String ort) {
        this.strasse = strasse;
        this.hausnummer = hausnummer;
        this.plz = plz;
        this.ort = ort;
    }

    @Override
    public String toString() {

        return String.format("[%d]: %s %s, %s %s", id, plz, ort, strasse, hausnummer);
    }

    public String show() {

        return String.format("%s - [%d] %s, %s", toString(), kundebi.getId(), kundebi.getNachname(), kundebi.getVorname());
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public KundeBi getKundebi() {
        return kundebi;
    }

    public void setKundebi(KundeBi kundebi) {
        this.kundebi = kundebi;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getHausnummer() {
        return hausnummer;
    }

    public void setHausnummer(String hausnummer) {
        this.hausnummer = hausnummer;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }
}
