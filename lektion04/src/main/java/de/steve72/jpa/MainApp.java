package de.steve72.jpa;

import de.steve72.jpa.model.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;


public class MainApp {

    public static void main(String... args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlPU");
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        Sparkonto spar = new Sparkonto("Privatbank Bauersheimer", new BigDecimal(0.2f));
        em.persist(spar);

        Sparkonto edeka = new Sparkonto("Volksbank", new BigDecimal(0.1f));
        em.persist(edeka);

        System.out.println(spar);
        System.out.println(edeka);

        Girokonto giro = new Girokonto("Deutsches Bank Polen", new BigDecimal(0));
        em.persist(giro);

        SparkontoJOINED sparkontoJOINED = new SparkontoJOINED("Paulfelzbank", new BigDecimal(0.1f));
        em.persist(sparkontoJOINED);

        GirokontoJOINED girokontoJOINED = new GirokontoJOINED("Hab & Gier", new BigDecimal(0.1f));
        em.persist(girokontoJOINED);

        SparkontoTBC sparkontoTBC = new SparkontoTBC("Spasskasse", new BigDecimal(0f));
        em.persist(sparkontoTBC);

        em.getTransaction().commit();

        em.clear();
        emf.close();
    }
}
