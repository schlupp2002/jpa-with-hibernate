package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;


@Entity
public class Buchung {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private BigDecimal betrag;

    @Temporal(TemporalType.TIMESTAMP)
    private Date datum;

    private String text;

    public Buchung() {
    }

    public Buchung(BigDecimal betrag, String text) {

        this.betrag = betrag;
        this.text = text;
        this.datum = new Date();
    }

    @Override
    public String toString() {
        return String.format("%s: %10s %s", datum, betrag, text);
    }
}
