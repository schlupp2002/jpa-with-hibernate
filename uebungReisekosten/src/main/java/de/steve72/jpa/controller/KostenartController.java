package de.steve72.jpa.controller;

import de.steve72.jpa.model.Kostenart;
import de.steve72.jpa.util.BigDecimalUtil;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * KostenartController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com
 */
public class KostenartController {

    public static Kostenart createKostenart(String kostenart, String value) {

        return new Kostenart(kostenart, BigDecimalUtil.fromString(value));
    }


    public static Kostenart createKostenart(String kostenart, Integer value) {

        return new Kostenart(kostenart, BigDecimalUtil.fromInteger(value));
    }


    public static Kostenart createKostenart(String kostenart, Double value) {

        return new Kostenart(kostenart, BigDecimalUtil.fromDouble(value));
    }


    public static List<Kostenart> fromCSV(String filename) {

        List<Kostenart> kostenarten = new ArrayList<>();

        URL data = Kostenart.class.getResource("/" + filename);

        try (BufferedReader in = new BufferedReader(new InputStreamReader(data.openStream()))) {

            String inputLine;

            while((inputLine = in.readLine()) != null) {
                String[] parts = inputLine.split(",");
                kostenarten.add(KostenartController.createKostenart(parts[0], parts[1]));
            }

        } catch (IOException ex) {

            System.err.println(ex.getMessage());
        }

        return kostenarten;
    }


    public static List<Kostenart> fromXML() {

        // @Todo: Einlesen der Kostenarten aus einem XML-File
        return null;
    }
}
