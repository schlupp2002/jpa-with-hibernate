package de.steve72.jpa.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Column;

@Embeddable
public class CountryLanguagePK implements Serializable {

	@Column(columnDefinition="char(3)")
	private String countrycode;

	@Column(columnDefinition="char(30)")
	private String language;

	public CountryLanguagePK() {

	}

	public CountryLanguagePK(String cc, String lang) {

		this.countrycode=cc;
		this.language=lang;
	}

	public String getCountrycode() {

		return this.countrycode;
	}

	public String getLanguage() {

		return this.language;
	}	

	@Override
	public int hashCode() {

		int result = 5381;

		result = result * 37 + this.countrycode.hashCode();
		result = result * 37 + this.language.hashCode();

		return result;
	}

	@Override
	public boolean equals(Object other) {

		if(other == null)
			return false;

		if(other == this)
			return true;

		CountryLanguagePK clpk = (CountryLanguagePK) other;

		return this.countrycode.equals(clpk.getCountrycode()) && this.language.equals(clpk.getLanguage());
	}
}
