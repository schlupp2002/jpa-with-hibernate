package de.steve72.jpa.model;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;


/**
 * Created by bauer on 15.06.16.
 */
public class KundeListener {

    @PrePersist
    public void vorEinfuegen(Kunde kunde) {

        System.out.println("Listener PRE: id=" + kunde.getId());
    }

    @PostPersist
    public void nachEinfuegen(Kunde kunde) {

        System.out.println("Listener POST: id=" + kunde.getId());
    }

}
