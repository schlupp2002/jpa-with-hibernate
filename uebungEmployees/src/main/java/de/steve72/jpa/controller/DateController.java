package de.steve72.jpa.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * DateController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class DateController {

    public static Date getDateFromString(String date) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date retDate = null;

        try {
            retDate = simpleDateFormat.parse(date);
        } catch (ParseException e) {

            System.err.println(e.getMessage());
        }

        return retDate;
    }
}
