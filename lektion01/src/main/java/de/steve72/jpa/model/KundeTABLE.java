package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.TableGenerator;

@Entity
public class KundeTABLE {

  @Id
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "kundeGen")
  @TableGenerator(
    name="kundeGen",
    table="ID_GEN",
    pkColumnName="GEN_KEY",
    valueColumnName="GEN_VALUE",
    pkColumnValue="KUNDE_ID",
    initialValue=50,
    allocationSize=50)
  private long id;
  
  private String vorname;
  private String nachname;

  public KundeTABLE() {

    this.vorname="John";
    this.nachname="Doe";  
  }

  public KundeTABLE(String vorname, String nachname) {

    this.vorname = vorname;
    this.nachname = nachname;
  }

  @Override
  public String toString() {

    return String.format("%s: %s, %s", this.id, this.nachname, this.vorname);
  }
}
