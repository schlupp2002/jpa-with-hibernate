package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.EmbeddedId;

@Entity
public class Ma2P {

	// Schlüsselklassenobjekt
	@EmbeddedId
	private KeyMa2P keyma2p;

	private String position;
	private String beschreibung;

	// der Konstruktor hat zwei Werte für den zusammengesetzten
	// Schlüssel fest in sich verdrahtet
	public Ma2P() {

		this.keyma2p = new KeyMa2P(101, 400);
		this.position = "Bla Bla";
		this.beschreibung = "Blubb Blubb";
	}
}
