package de.steve72.jpa.model;

import java.util.Date;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class PojoColumn {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="poCo_vorname", length=40)
	private String vorname;

	@Column(columnDefinition="char(30) not null")
	private String nachname;

	@Temporal(TemporalType.DATE)
	@Column(columnDefinition="timestamp default current_timestamp", insertable=false, updatable=false)
	private Date eintritt;
	
	@Column(precision=7, scale=2)
	private BigDecimal gehalt;

	public PojoColumn() {

		this.vorname = "John";
		this.nachname ="Doe";
		this.eintritt = new Date();
		this.gehalt = new BigDecimal(2200.75);
	}
}
