package de.steve72.jpa.model;

import java.io.Serializable;


public class DepartmentManagerPK implements Serializable {

    private Employee employee;
    private Department department;

    //region Konstruktoren
    public DepartmentManagerPK() {
    }

    public DepartmentManagerPK(Employee employee, Department department) {

        this.employee = employee;
        this.department = department;
    }
    //endregion

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        DepartmentManagerPK that = (DepartmentManagerPK) o;

        if (!employee.equals(that.employee))
            return false;

        return department.equals(that.department);

    }

    @Override
    public int hashCode() {

        int result = 5381;

        result *= 37 + employee.hashCode();
        result *= 37 + department.hashCode();

        return result;
    }
}