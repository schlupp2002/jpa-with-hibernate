package de.steve72.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;


@Entity
public class Postleitzahl {

    @Id
    @Column(length = 5)
    private String plz;

    @Column(length = 40)
    private String ort;

    // 1:N Beziehung
    @OneToMany(mappedBy = "plz", cascade = CascadeType.PERSIST)
    private Set<Mitarbeiter> mitarbeiter;


















    public Postleitzahl() {

        this("XXXXX", "Unknown");
    }

    public Postleitzahl(String plz, String ort) {
        this.plz = plz;
        this.ort = ort;
    }

    @Override
    public String toString() {
        return String.format("%s %s", plz, ort);
    }
}
