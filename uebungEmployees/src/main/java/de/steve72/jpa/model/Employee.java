package de.steve72.jpa.model;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "tbl_employees")
public class Employee {

    public enum GenderType {

        M, F;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer emp_no;

    // die 1:N-Beziehung mit "Salary"
    @OneToMany(mappedBy = "emp_no", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Salary> salary;

    // die 1:N-Beziehung mit "Title"
    @OneToMany(mappedBy = "emp_no", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Title> title;

    // die N:M-Beziehung mit "Department"
    @OneToMany(mappedBy = "employee", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<DepartmentManager> departmentManager;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date birth_date;

    @Column(length = 14, nullable = false)
    private String first_name;

    @Column(length = 16, nullable = false)
    private String last_name;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "enum('M','F')", nullable = false)
    private GenderType gender;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date hire_date;

    // region Konstruktoren
    public Employee() {

        this.salary = new HashSet<>();
        this.title = new HashSet<>();
        this.departmentManager = new HashSet<>();
    }

    public Employee(String first_name, String last_name, GenderType gender, Date birth_date, Date hire_date) {

        this();
        this.first_name = first_name;
        this.last_name = last_name;
        this.gender = gender;
        this.birth_date = birth_date;
        this.hire_date = hire_date;
    }
    // endregion

    // region Getter/Setter
    public Integer getEmp_no() {
        return emp_no;
    }

    public Set<Salary> getSalary() {
        return salary;
    }

    public Set<Title> getTitle() {
        return title;
    }

    public Set<DepartmentManager> getDepartmentManager() {
        return departmentManager;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public Date getHire_date() {
        return hire_date;
    }

    public void setHire_date(Date hire_date) {
        this.hire_date = hire_date;
    }

    // endregion

    @Override
    public String toString() {

        return String.format("%11d %16s, %14s (%s) %10s %10s",
              emp_no,
              last_name,
              first_name,
              gender,
              birth_date,
              hire_date);
    }
}
