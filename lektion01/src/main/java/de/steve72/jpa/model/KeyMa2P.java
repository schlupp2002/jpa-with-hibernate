package de.steve72.jpa.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

/** 
 * Schlüsselklasse für die Entität "Ma2P"
 * 
 */

@Embeddable
public class KeyMa2P implements Serializable {

	// Schlüsselattribute für den zusammengesetzten Schlüssel
	private long maid;
	private long pid;

	public KeyMa2P() {

	}

	public KeyMa2P(long maid, long pid) {

		this.maid = maid;
		this.pid = pid;
	}

	public long getMaid() {
	
		return this.maid;
	}

	public long getPid() {

		return this.pid;
	}

	@Override
	public boolean equals(Object other) {

		// Der Vergleich mit "nichts" ist immer falsch
		if(other == null)
			return false;

		// Der Vergleich mit sich selbst ist immer richtig
		if(other == this)		
			return true;

		// der eigentliche Vergleich
		KeyMa2P tmp = (KeyMa2P) other;

		return tmp.getMaid() == this.maid && tmp.getPid() == this.pid;
	}	

	@Override
	public int hashCode() {

		int result = 5381;

		result = result * 37 + (int)this.maid;
		result = result * 37 + (int)this.pid;

		return result;
	}
}
