package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;


@Entity
public class KundeIDENTITY {

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private long id;
  
  private String vorname;
  private String nachname;

  public KundeIDENTITY() {

    this.vorname="John";
    this.nachname="Doe";  
  }

  public KundeIDENTITY(String vorname, String nachname) {

    this.vorname = vorname;
    this.nachname = nachname;
  }

  @Override
  public String toString() {

    return String.format("%s: %s, %s", this.id, this.nachname, this.vorname);
  }
}
