package de.steve72.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String vorname;
    private String nachname;

    @OneToOne
    @JoinColumn(name="partner", unique = true)
    private Person partner;

    @OneToOne(mappedBy = "partner")
    private Person partnerInv;

    public Person() {
    }

    public Person(String vorname, String nachname) {
        this.vorname = vorname;
        this.nachname = nachname;
    }

    public Person getPartner() {
        return partner;
    }

    public void setPartner(Person partner) {
        this.partner = partner;
    }

    public void heiratet(Person partner) {

        if(this == partner)
            return;

        if(partner.getPartner() != null || this.partner != null) {
            System.out.println("So geht das hier nicht in DE");
            return;
        }

        this.setPartner(partner);
        partner.setPartner(this);
    }

    public void scheidetSich() {

        Person tmp = this.getPartner();

        this.setPartner(null);
        tmp.setPartner(null);
    }
}
