package de.steve72.jpa.controller;

import de.steve72.jpa.model.Postleitzahl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * PlzController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com
 */
public class PlzController {

    public static Postleitzahl createPlz(String osmid, String ort, String plz, String bundesland) {

        Long osmidLong = Long.valueOf(osmid);
        String plzZerofill = String.format("%05d", Integer.valueOf(plz));

        return new Postleitzahl(osmidLong, plzZerofill, ort, bundesland);
    }


    public static List<Postleitzahl> fromCSV(String filename) {

        List<Postleitzahl> postleitzahlen = new ArrayList<>();

        URL data = Postleitzahl.class.getResource("/" + filename);

        try (BufferedReader in = new BufferedReader(new InputStreamReader(data.openStream()))) {

            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                String[] parts = inputLine.split(",");
                postleitzahlen.add(PlzController.createPlz(parts[0], parts[1], parts[2], parts[3]));
            }

        } catch (IOException ex) {

            System.err.println(ex.getMessage());
        }

        return postleitzahlen;
    }
}
