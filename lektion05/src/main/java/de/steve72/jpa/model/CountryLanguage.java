package de.steve72.jpa.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@NamedQuery(name="CountryLanguage.count", query="select count(distinct cl) from CountryLanguage cl")
public class CountryLanguage {

    @EmbeddedId
    private CountryLanguagePK clpk;

    @Column(columnDefinition = "float(4,1)")
    private Float percentage;

    @ManyToOne
    @JoinColumn(
          name = "countrycode",
          insertable = false,
          updatable = false
    )
    private Country countrycode;

    public CountryLanguage() {
    }

    public CountryLanguage(CountryLanguagePK clpk) {
        this.clpk = clpk;
    }

    public Country getCountry() {
        return countrycode;
    }

    public void setCountry(Country country) {
        this.countrycode = country;
    }

    public Float getPercentage() {
        return percentage;
    }

    public void setPercentage(Float percentage) {
        this.percentage = percentage;
    }

    @Override
    public String toString() {
        return "CountryLanguage{" +
              "clpk=" + clpk +
              ", percentage=" + percentage +
              ", countrycode=" + countrycode +
              '}';
    }
}
