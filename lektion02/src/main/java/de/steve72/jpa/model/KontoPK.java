package de.steve72.jpa.model;

import javax.persistence.Embeddable;
import java.io.Serializable;


@Embeddable
public class KontoPK implements Serializable {

    private String iban;
    private String bic;

    public KontoPK() {

    }

    public KontoPK(String iban, String bic) {

        this.iban = iban;
        this.bic = bic;

    }

    @Override
    public boolean equals(Object other) {

        if (other == null)
            return false;

        if (other == this)
            return true;

        KontoPK tmp = (KontoPK) other;

        return this.iban.equals(tmp.getIban()) && this.bic.equals(tmp.getBic());
    }

    @Override
    public int hashCode() {

        int result = 5381;

        result = result * 37 + this.iban.hashCode();
        result = result * 37 + this.bic.hashCode();

        return result;
    }

    public String getIban() {
        return iban;
    }

    public String getBic() {
        return bic;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }
}
