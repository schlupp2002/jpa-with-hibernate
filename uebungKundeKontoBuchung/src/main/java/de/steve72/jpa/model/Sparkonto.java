package de.steve72.jpa.model;

import javax.persistence.Entity;
import java.math.BigDecimal;


@Entity
public class Sparkonto extends Konto {

    private BigDecimal zinssatz;

    //region Konstruktoren
    public Sparkonto() {
        super();
        this.zinssatz = BigDecimal.ZERO.setScale(2);
    }

    public Sparkonto(String bankname, BigDecimal zinssatz) {

        super(bankname);
        this.zinssatz = zinssatz;
    }
    //endregion

    //region Getter/Setter
    public BigDecimal getZinssatz() {
        return zinssatz;
    }

    public void setZinssatz(BigDecimal zinssatz) {
        this.zinssatz = zinssatz;
    }
    //endregion
}
