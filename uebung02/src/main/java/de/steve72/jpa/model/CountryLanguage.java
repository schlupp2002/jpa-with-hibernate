package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EmbeddedId;
import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

@Entity
@Table(name="tbl_countrylanguage")
public class CountryLanguage {

	public enum TrueFalse {

		T, F;
	}

	@EmbeddedId
	private CountryLanguagePK clpk;	

	@Enumerated(EnumType.STRING)
	@Column(columnDefinition="enum('t','f') default 'f'", nullable=false)
	private TrueFalse isOfficial;

	@Column(columnDefinition="float(4,1) default 0.0")
	private float percentage;

	public CountryLanguage() {

	}

	public CountryLanguage(CountryLanguagePK clpk, TrueFalse tf, float percentage) {

		this.clpk = clpk;
		this.isOfficial=tf;
		this.percentage=percentage;
	}
}
