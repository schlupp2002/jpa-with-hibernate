package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;


@Entity
public class KundeAUTO {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private long id;
  
  private String vorname;
  private String nachname;

  public KundeAUTO() {

    this.vorname="John";
    this.nachname="Doe";  
  }

  public KundeAUTO(String vorname, String nachname) {

    this.vorname = vorname;
    this.nachname = nachname;
  }

  @Override
  public String toString() {

    return String.format("%s: %s, %s", this.id, this.nachname, this.vorname);
  }
}
