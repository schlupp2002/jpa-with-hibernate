package de.steve72.jpa.util;

import java.math.BigDecimal;


/**
 * BigDecimalUtil
 *
 * @author Steffen Bauer <schlupp2002@gmail.com
 */
public class BigDecimalUtil {

    public static BigDecimal fromString(String value) {

        return new BigDecimal(value).setScale(2);
    }

    public static BigDecimal fromInteger(Integer value) {

        return new BigDecimal(value).setScale(2);
    }

    public static BigDecimal fromDouble(Double value) {

        return new BigDecimal(value).setScale(2);
    }
}
