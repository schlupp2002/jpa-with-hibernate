package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


/**
 * Created by bauer on 16.06.16.
 */
@Entity
@IdClass(PositionPK.class)
public class Position {

    @Id
    @ManyToOne
    @JoinColumn(name="rgnr")
    private Rechnung rechnung;

    @Id
    @ManyToOne
    @JoinColumn(name="kostenart")
    private Kostenart kostenart;


    private Integer anzahl;

    // Konstruktoren
    public Position() {
    }

    public Position(Rechnung rechnung, Kostenart kostenart, Integer anzahl) {
        this.rechnung = rechnung;
        this.kostenart = kostenart;
        this.anzahl = anzahl;
    }
}
