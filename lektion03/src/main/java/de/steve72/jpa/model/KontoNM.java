package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class KontoNM {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer kontonummer;

    private String iban;

    public KontoNM() {

        this.iban ="DE123445122 XX";
    }

    public KontoNM(String iban) {
        this.iban = iban;
    }

    @Override
    public String toString() {
        return String.format("[%d]: Fake-IBAN: %s", kontonummer, iban);
    }
}
