package de.steve72.jpa.model;

import java.io.Serializable;

public class KundeId implements Serializable {

  private String abt;
  private int num;

  public KundeId() {

  }

  public KundeId(String abt, int num) {

    this.abt=abt;
    this.num=num;
  }
  
  @Override
  public boolean equals(Object other) {

    if(other==null)
      return false;
  
    if(other==this)
      return true;

    KundeId tmp = (KundeId)other;

    return this.abt.equals(tmp.getAbt()) && this.num==tmp.getNum();
  }

  @Override
  public int hashCode() {

    int result = 5381;

    result = result * 37 + this.abt.hashCode();
    result = result * 37 + this.num;

    return result;
  }
  
  public String getAbt() {
  
    return this.abt;
  }

  public int getNum() {

    return this.num;
  }
}
