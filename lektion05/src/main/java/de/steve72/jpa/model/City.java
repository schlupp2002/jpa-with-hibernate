package de.steve72.jpa.model;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@NamedQueries({
      @NamedQuery(name = "City.showCities", query = "select c from City c"),
      @NamedQuery(name = "City.showCityByName", query = "select c from City c where c.name = :name"),
      @NamedQuery(name="City.byNameAndPopulation", query = "select c from City c where c.name like :name and c.population > :population"),
      @NamedQuery(name="City.count", query="select count(c) from City c"),
      @NamedQuery(name="City.findById", query = "select c from City c where c.id = :id")
    })
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(columnDefinition = "char(35) default ''", nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn( name="countrycode", insertable = false, updatable = false)
    private Country countrycode;

    @Column(columnDefinition = "char(20) default ''", nullable = false)
    private String district;

    @Column(columnDefinition = "int default 0", nullable = false)
    private int population;

    public City() {

    }

    public City(String name, Country country, String district, int population) {
        this.name = name;
        this.countrycode = country;
        this.district = district;
        this.population = population;
    }

    @Override
    public String toString() {

        return String.format("%05d %-35s %-3s %-20s %10d", id, name, countrycode.getCode(), district, population);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(Country countrycode) {
        this.countrycode = countrycode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }
}
