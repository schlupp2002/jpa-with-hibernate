package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.EmbeddedId;

@Entity
public class KundeEMBEDDEDID {

  @EmbeddedId
  private KundeIdEmbedded id;

  private String vorname;
  private String nachname;

  public KundeEMBEDDEDID() {
  
  }

  public KundeEMBEDDEDID(KundeIdEmbedded id, int num, String vorname, String nachname) {

    this.id=id;
  
    this.vorname=vorname;
    this.nachname=nachname;
  }

  @Override
  public String toString() {

    return String.format("%s: [%s, %d] %s, %s", this.getClass().getSimpleName(), this.id,   this.nachname, this.vorname);
  }
}
