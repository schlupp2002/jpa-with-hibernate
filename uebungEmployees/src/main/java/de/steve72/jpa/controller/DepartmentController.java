package de.steve72.jpa.controller;

import de.steve72.jpa.model.Department;

/**
 * DepartmentController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class DepartmentController {

    public static Department getDepartment(String dept_no, String dept_name) {

        return new Department(dept_no, dept_name);
    }
}
