package de.steve72.jpa.controller;

import de.steve72.jpa.model.Salary;
import de.steve72.jpa.model.SalaryPK;

import java.util.Date;

/**
 * SalaryController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class SalaryController {

    public static Salary getSalary(Integer emp_no, String from_date, Integer salary) {

        return new Salary(new SalaryPK(emp_no, DateController.getDateFromString(from_date)), salary);
    }
}
