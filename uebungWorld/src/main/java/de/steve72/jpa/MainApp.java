package de.steve72.jpa;

import de.steve72.jpa.model.Country;
import de.steve72.jpa.model.CountryLanguage;
import de.steve72.jpa.model.CountryLanguagePK;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class MainApp {

    public static void main(String... args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlPU");
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        em.getTransaction().commit();

        em.clear();
        emf.close();
    }
}
