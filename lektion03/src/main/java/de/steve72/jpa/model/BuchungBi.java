package de.steve72.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Created by bauer on 16.06.16.
 */
@Entity
public class BuchungBi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(precision = 10, scale = 2)
    private BigDecimal betrag;

    @Temporal(TemporalType.TIMESTAMP)
    private Date datum;

    private String text;

    @ManyToOne
    @JoinColumn(name = "konto")
    private KontoBi konto;

    public BuchungBi() {
    }

    public BuchungBi(BigDecimal betrag, String text) {

        this.betrag = betrag;
        this.text = text;

        this.datum = new Date();
    }

    public void setKonto(KontoBi konto) {
        this.konto = konto;
    }

    @Override
    public String toString() {
        return String.format("[%d]: %10s, %s %s", id, betrag, datum, text);
    }
}
