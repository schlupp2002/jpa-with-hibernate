package de.steve72.jpa.controller;

import de.steve72.jpa.model.Title;
import de.steve72.jpa.model.TitlePK;

/**
 * TitleController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class TitleController {

    public static Title getTitle(Integer emp_no, String title, String from_date, String to_date) {

        return new Title(new TitlePK(emp_no, title, DateController.getDateFromString(from_date)), DateController.getDateFromString(to_date));
    }
}
