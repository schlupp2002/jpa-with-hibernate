package de.steve72.jpa;

import de.steve72.jpa.model.City;
import de.steve72.jpa.model.CountryLanguage;
import de.steve72.jpa.model.CountryLanguagePK;
import de.steve72.jpa.model.Country;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MainApp {

  public static void main(String ... args) {

	City city = new City("Köthen", "DEU", "Sachsen-Anhalt", 26000);

	CountryLanguagePK clpk = new CountryLanguagePK("DEU", "german");
	CountryLanguage cl = new CountryLanguage( clpk, CountryLanguage.TrueFalse.T, 80.0f);

	Country country = new Country();

    // 1. Wir benötigen für unsere persistenten Objekte einen Verwalter
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlPU");
    EntityManager em = emf.createEntityManager();
        
    // 2. der EntityManager soll das Objekt in der Datenbank speichern
    em.getTransaction().begin();

	em.persist(city);
	em.persist(cl);
	em.persist(country);

    em.getTransaction().commit();
    
    // 3. Aufräumen
    em.clear();
    emf.close();
  }
}
