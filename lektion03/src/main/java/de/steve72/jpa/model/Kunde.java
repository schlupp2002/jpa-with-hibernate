package de.steve72.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;


/**
 * Created by bauer on 15.06.16.
 */
@Entity
public class Kunde {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String vorname;
    private String nachname;

    @Temporal(TemporalType.DATE)
    private Date gebTag;

    // Für die 1:1-Beziehung ...
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name="adresseID", unique = true)
    private Adresse adresse;

    // Konstruktoren
    public Kunde() {
    }

    public Kunde(String vorname, String nachname, Date gebTag) {

        this.vorname = vorname;
        this.nachname = nachname;
        this.gebTag = gebTag;
    }

    @Override
    public String toString() {
        return String.format("[%d]: %s, %s geb. %s wohnt in %s",
                this.id, this.nachname, this.vorname, this.gebTag, this.adresse);
    }

    // Getter/Setter
    public int getId() {
        return id;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public Date getGebTag() {
        return gebTag;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public void setGebTag(Date gebTag) {
        this.gebTag = gebTag;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }
}
