package de.steve72.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;


@Entity
public class KontoNMBi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String fakeIban;

    @ManyToMany(mappedBy = "konten", cascade = CascadeType.PERSIST)
    private Set<KundeNMBi> kunden;

    public KontoNMBi() {

        this.kunden = new HashSet<>();
        this.fakeIban = "Fake-Iban: DE 12345 XX";
    }

    public void addKunde(KundeNMBi k) {

        this.kunden.add(k);
    }

    @Override
    public String toString() {
        return String.format("Konto [%d]: %s", id, fakeIban);
    }

    public String zeigeKunden() {

        StringBuilder sb = new StringBuilder();

        sb.append(String.format("Zugriff auf Konto %d haben: %n", id));

        for (KundeNMBi ku : kunden) {

            sb.append(String.format("[%d]: %s, %s", ku.getId(), ku.getVorname(), ku.getNachname()));
            sb.append("\n");
        }

        return sb.toString();
    }
}
