package de.steve72.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;


@Entity
@NamedQueries(
      {
            @NamedQuery(name="Postleitzahl.count", query = "select count(p) from Postleitzahl p")
      }
)
public class Postleitzahl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Long osmid;

    @Column(length = 5)
    private String plz;

    @Column(length = 60)
    private String ort;

    @Column(length = 40)
    private String bundesland;

    // 1:N Beziehung
    @OneToMany(mappedBy = "osmid", cascade = CascadeType.PERSIST)
    private Set<Mitarbeiter> mitarbeiter;


    public Postleitzahl() {

        this.mitarbeiter = new HashSet<>();
    }


    public Postleitzahl(Long osmid, String plz, String ort, String bundesland) {

        this.osmid = osmid;
        this.plz = plz;
        this.ort = ort;
        this.bundesland = bundesland;
    }


    @Override
    public String toString() {

        return String.format("%s %s", plz, ort);
    }


    public Long getOsmid() {

        return osmid;
    }


    public String getPlz() {

        return plz;
    }


    public void setPlz(String plz) {

        this.plz = plz;
    }


    public String getOrt() {

        return ort;
    }


    public void setOrt(String ort) {

        this.ort = ort;
    }


    public String getBundesland() {

        return bundesland;
    }


    public void setBundesland(String bundesland) {

        this.bundesland = bundesland;
    }


    public Set<Mitarbeiter> getMitarbeiter() {

        return mitarbeiter;
    }


    public void setMitarbeiter(Set<Mitarbeiter> mitarbeiter) {

        this.mitarbeiter = mitarbeiter;
    }
}
