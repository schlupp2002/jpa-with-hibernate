use jpawh;

drop table if exists KundeTABLE;

create table KundeTABLE (

  id          bigint unsigned not null primary key,
  vorname     varchar(40),
  nachname    varchar(40),
  createdAt   timestamp
);
