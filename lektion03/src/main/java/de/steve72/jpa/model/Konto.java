package de.steve72.jpa.model;

import org.hibernate.annotations.CollectionId;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Konto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int kontonummer;

    @OneToMany
    private Set<Buchung> buchungen;

    public Konto() {

        this.buchungen = new HashSet<>();

    }

    public int getKontonummer() {
        return kontonummer;
    }

    public Set<Buchung> getBuchungen() {
        return buchungen;
    }

    public void setBuchungen(Set<Buchung> buchungen) {
        this.buchungen = buchungen;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append("Kontoauszug\n");

        for(Buchung b: this.buchungen) {

            sb.append(b);
            sb.append("\n");        // Zeilenumbruch (newline)
        }

        return sb.toString();
    }
}
