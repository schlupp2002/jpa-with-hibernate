package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

@Entity
public class PojoEnum {

	public enum Anrede {

		HERR, FRAU, DR, PROF;
	}

	public enum Komplex {

		LARGE(1000), MEDIUM(500), SMALL(100);

		private final int value;

		Komplex(int v) {

			value=v;
		}
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Enumerated(EnumType.ORDINAL)
	private Anrede anredeMale;
	
	@Enumerated(EnumType.STRING)
	private Anrede anredeFemale;


	
	@Enumerated(EnumType.ORDINAL)
	private Komplex large;
	
	@Enumerated(EnumType.STRING)
	private Komplex medium;
	
	@Enumerated(EnumType.STRING)
	private Komplex small;


	public PojoEnum() {

		this.anredeMale = Anrede.HERR;
		this.anredeFemale = Anrede.FRAU;

		this.large = Komplex.LARGE;
		this.medium = Komplex.MEDIUM;
		this.small = Komplex.SMALL;
	}
}
