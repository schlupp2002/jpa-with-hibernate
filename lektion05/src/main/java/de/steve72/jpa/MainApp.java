package de.steve72.jpa;

import de.steve72.jpa.model.City;
import de.steve72.jpa.model.Country;
import de.steve72.jpa.model.CountryLanguage;
import de.steve72.jpa.model.CountryLanguagePK;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;


public class MainApp {

    public static void main(String... args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlPU");
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        // eine namedQuery verwenden
        Query query = em.createNamedQuery("City.showCities", City.class);
        List<City> cities = query.getResultList();

        // alle Elemente der Liste ausgeben
        for (City c : cities) {

            System.out.println(c);
        }

        // eine einzelne Stadt abfragen
        query = em.createNamedQuery("City.showCityByName", City.class).setParameter("name", "Berlin");
        cities = query.getResultList();
        System.out.println(cities.get(0));

        // unbenannte Abfragen
        query = em.createQuery("select c from Country c", Country.class);
        List<Country> countries = query.getResultList();

        for (Country c : countries) {

            System.out.println(c);
        }

        // unbenannte Abfrage ohne Typparameter
        query = em.createQuery("select c from Country c");
        List<Country> untypisch = (List<Country>) query.getResultList();

        System.out.println(untypisch.get(0));

        untypisch.get(0).setName("blabla ");

        // Parameter in Abfragen verwenden
        query = em.createNamedQuery("City.byNameAndPopulation", City.class).setParameter("name", "K%").setParameter("population", 1_000_000);
        cities = query.getResultList();

        for (City c : cities) {

            System.out.println(c);
        }

        query = em.createNamedQuery("Country.byNameAndPopulation", Country.class).setParameter(1, "G%").setParameter(2, 1_000_000);
        countries = query.getResultList();

        for (Country c : countries) {

            System.out.println(c);
        }

        query = em.createNamedQuery("Country.byName", Country.class).setParameter("name", "Germany");
        countries = query.getResultList();

        // das Country-Objekt abfragen
        Country country = countries.get(0);

        // alle Landessprachen des Landes abfragen
        for (CountryLanguage cl : country.getLanguages()) {

            System.out.println(cl);
        }

        // alle Städte des Landes abfragen
        for (City ci : country.getCities()) {

            System.out.println(ci);
        }

        // eine Abfrage unter Ausnutzung einer nicht gemappten JOIN

        query = em.createQuery("select c from City c, Country co where c.id=co.capital and co.name='Germany' ");
        City capital = (City) query.getSingleResult();

        System.out.println("Die Hauptstadt von Germany ist: " + capital);

// ######################################################################################################
        // Übung: Anzahl der Städte, Länder, Sprachen
        query = em.createNamedQuery("City.count");
        Long ciCount = (Long) query.getSingleResult();

        query = em.createNamedQuery("Country.count");
        Long coCount = (Long) query.getSingleResult();

        query = em.createNamedQuery("CountryLanguage.count");
        Long clCount = (Long) query.getSingleResult();

        System.out.printf("Städte: %5d, Länder: %5d , versch. Sprachen: %5d", ciCount, coCount, clCount);

        // Übung: die durchschnittliche Population der Länder
        query = em.createNamedQuery("Country.avgPop");
        Double avgPop = (Double) query.getSingleResult();

        System.out.printf("durchnittliche Ew-Zahl der Länder: %11.2f", avgPop);

        // Übung: die (ersten 20) Haupstädte der Länder
        query = em.createNamedQuery("Country.findAllWithCapitals", Country.class).setFirstResult(0).setMaxResults(20);
        countries = query.getResultList();

        System.out.printf("%-52s: %-35s%n", "Land", "Hauptstadt");
        for (Country c : countries) {

            query = em.createNamedQuery("City.findById").setParameter("id", c.getCapital());
            City cap = (City) query.getSingleResult();
            System.out.printf("%-52s: %-35s%n", c.getName(), cap.getName());
        }

        em.getTransaction().commit();

        em.clear();
        emf.close();
    }
}
