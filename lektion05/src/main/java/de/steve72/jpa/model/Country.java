package de.steve72.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;


@Entity
@NamedQueries({
      @NamedQuery(name = "Country.byNameAndPopulation", query = "select c from Country c where c.name like ?1 and c.population > ?2"),
      @NamedQuery(name = "Country.byName", query = "select c from Country c where c.name= :name"),
      @NamedQuery(name = "Country.count", query = "select count(c) from Country c"),
      @NamedQuery(name = "Country.avgPop", query = "select avg(c.population) from Country c"),
      @NamedQuery(name = "Country.findAll", query = "select c from Country c"),
      @NamedQuery(name = "Country.findAllWithCapitals", query = "select c from Country c where c.capital is not null")
})
public class Country {

    @Id
    @Column(length = 3)
    private String code;

    @Column(length = 52)
    private String name;

    private Integer capital;

    @OneToMany(mappedBy = "countrycode")
    private Set<City> cities;

    @OneToMany(mappedBy = "countrycode")
    private Set<CountryLanguage> languages;

    private Integer population;

    public Country() {
        this.cities = new HashSet<>();
        this.languages = new HashSet<>();
    }

    public Country(String code, String name, Integer population) {
        this();
        this.code = code;
        this.name = name;
        this.population = population;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public Set<CountryLanguage> getLanguages() {
        return languages;
    }

    public Set<City> getCities() {
        return cities;
    }

    public Integer getCapital() {
        return capital;
    }

    public void setCapital(Integer capital) {
        this.capital = capital;
    }

    @Override
    public String toString() {
        return "Country{" +
              "code='" + code + '\'' +
              ", name='" + name + '\'' +
              ", population=" + population +
              '}';
    }
}
