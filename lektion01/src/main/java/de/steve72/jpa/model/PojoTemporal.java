package de.steve72.jpa.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class PojoTemporal {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Temporal(TemporalType.TIME)
	private Date terminA;

	@Temporal(TemporalType.DATE)
	private Date terminB;

	@Temporal(TemporalType.TIMESTAMP)
	private Date terminC;

	public PojoTemporal() {

		this.terminA = new Date();
		this.terminB = new Date();
		this.terminC = new Date();
	}
}
