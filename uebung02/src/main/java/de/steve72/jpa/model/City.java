package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name="tbl_city")
public class City {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(columnDefinition="char(35) default ''", nullable=false)
	private String name;

	@Column(columnDefinition="char(3) default ''", nullable=false)
	private String countrycode;

	@Column(columnDefinition="char(20) default ''", nullable=false)
	private String district;

	@Column(columnDefinition="int default 0", nullable=false)
	private int population;


	public City() {

	}

	public City(String name, String countrycode, String district, int population) {

		this.name = name;
		this.countrycode = countrycode;
		this.district = district;
		this.population = population;
	}
}
