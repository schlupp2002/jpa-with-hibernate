package de.steve72.jpa.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;


@Entity
@DiscriminatorValue("Sparkonto")
public class Sparkonto  extends AKonto {

    private BigDecimal zinssatz;

    public Sparkonto() {

        super("noname");
        this.zinssatz = new BigDecimal(0);
    }

    public Sparkonto(BigDecimal zinssatz) {

        super("noname");
        this.zinssatz = zinssatz;
    }

    public Sparkonto(String bankname, BigDecimal zinssatz) {

        super(bankname);
        this.zinssatz = zinssatz;
    }
}
