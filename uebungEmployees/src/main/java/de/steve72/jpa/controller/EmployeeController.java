package de.steve72.jpa.controller;

import de.steve72.jpa.model.Employee;

/**
 * EmployeeController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class EmployeeController {

    public static Employee createEmployee(String first_name, String last_name, Employee.GenderType gender, String birth_date, String hire_date) {

        return new Employee(first_name, last_name, gender, DateController.getDateFromString(birth_date), DateController.getDateFromString(hire_date));
    }
}
