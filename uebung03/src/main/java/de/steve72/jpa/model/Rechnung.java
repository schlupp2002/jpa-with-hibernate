package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Rechnung {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer rgnr;

    @Temporal(TemporalType.DATE)
    private Date datum;

    // N-Seite der 1:N-(MA-RG)
    @ManyToOne
    @JoinColumn(name="maid")
    private Mitarbeiter mitarbeiter;

    // --------------------------------------------------------
    // 1-Seite der 1:N-Beziehung zur Assoziationsklasse
    @OneToMany(mappedBy ="rechnung")
    private Set<Position> positionen;

    // --------------------------------------------------------
    // Konstruktoren
    public Rechnung() {

        this.positionen = new HashSet<>();
    }

    public Rechnung(Date datum, Mitarbeiter mitarbeiter) {

        this();
        this.datum = datum;
        this.mitarbeiter = mitarbeiter;
    }

    // -------------------------------------------------------------
    // Getter/Setter

    public Integer getRgnr() {
        return rgnr;
    }

    public void setRgnr(Integer rgnr) {
        this.rgnr = rgnr;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Mitarbeiter getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(Mitarbeiter mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }
}
