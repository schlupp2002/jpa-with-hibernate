package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

@Entity
@Table(name="tbl_country")
public class Country {

	public enum Continent {

		ASIA, EUROPE, NORTH_AMERICA, AFRICA, OCEANIA, ANTARCTICA, SOUTH_AMERICA; 
	}

	@Id
	@Column(columnDefinition="char(3)")
	private String code;

	@Column(columnDefinition="char(52)")
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(columnDefinition="enum('ASIA', 'EUROPE', 'NORTH_AMERICA', 'AFRICA', 'OCEANIA', 'ANTARCTICA', 'SOUTH_AMERICA')")
	private Continent continent;

	@Column(columnDefinition="char(26)")
	private String region;

	@Column(columnDefinition="float(10,2)")
	private float surfaceArea;

	@Column(columnDefinition="smallint(6)")
	private int indepYear;

	@Column(columnDefinition="int")
	private int population;

	@Column(columnDefinition="float(3,1)")
	private float lifeExpectancy;

	@Column(columnDefinition="float(10,2)")
	private float gnp;

	@Column(columnDefinition="float(10,2)")
	private float gnpOld;

	@Column(columnDefinition="char(45)")
	private String localname;

	@Column(columnDefinition="char(45)")
	private String governmentForm;

	@Column(columnDefinition="char(60)")
	private String headOfState;

	@Column(columnDefinition="int")
	private int capital;

	@Column(columnDefinition="char(2)")
	private String code2;

	public Country() {

		 this.code="DEU";
		 this.name="Germany";
		 this.continent=Continent.NORTH_AMERICA;
		 this.region="Western Europe";
		 this.surfaceArea=9000.1f;
		 this.indepYear=1949;
		 this.population=80000000;
		 this.lifeExpectancy=75f;
		 this.gnp=1000f;
		 this.gnpOld=1000f;
		 this.localname="Bananenrepublik";
		 this.governmentForm="Fragwürdig";
		 this.headOfState="Bundesgaukler";
		 this.capital=100;
		 this.code2="DE";
	}	
}
