package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.EmbeddedId;

@Entity 
public class Konto {

	
	@EmbeddedId
	private KontoPK kontopk;

	private String inhaber;


	public Konto() {
		
	}

	public Konto(KontoPK kontopk, String inhaber) {
	

		this.kontopk = kontopk;
		this.inhaber = inhaber;
	}

	// Setter für den Inhaber
	public void setInhaber(String inhaber) {
		
		this.inhaber = inhaber;
	}

	@Override
	public String toString() {

		return String.format("[%s]: %s", this.kontopk, this.inhaber); 
	}
}
