package de.steve72.jpa.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class User {

    @EmbeddedId
    private UserId userid;

    @ManyToOne
    @JoinColumn(
          name="departmentId",
          insertable = false,
          updatable = false
    )
    private Department department;

    public User() {
    }

    public User(UserId userid) {
        this.userid = userid;
    }
}
