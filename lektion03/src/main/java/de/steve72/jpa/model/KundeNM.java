package de.steve72.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;


@Entity
public class KundeNM {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String vorname;
    private String nachname;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name="Kunde2Konto",
            joinColumns = { @JoinColumn(name="kunde")},
            inverseJoinColumns = { @JoinColumn(name="konto")}
    )
    private Set<KontoNM> konten;

    public KundeNM() {

        this.konten = new HashSet<>();
        this.vorname="John";
        this.nachname="Doe";
    }

    public void kundeZuKonto(KontoNM k) {

        this.konten.add(k);
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append(String.format("Kunde: %s, %s %n", nachname, vorname));

        for(KontoNM k: konten) {

            sb.append(k);
            sb.append("\n");
        }

        return sb.toString();
    }
}
