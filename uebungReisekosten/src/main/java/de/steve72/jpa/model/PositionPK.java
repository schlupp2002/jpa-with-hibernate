package de.steve72.jpa.model;

import java.io.Serializable;


/**
 * Created by bauer on 17.06.16.
 */
public class PositionPK implements Serializable {

    private Rechnung rechnung;
    private Kostenart kostenart;

    public PositionPK() {

    }

    public PositionPK(Rechnung rechnung, Kostenart kostenart) {

        this.rechnung = rechnung;
        this.kostenart = kostenart;
    }

    @Override
    public int hashCode() {

        int result = 5381;

        result = result * 37 + rechnung.getRgnr();
        result = result * 37 + kostenart.getKostenart().hashCode();

        return result;
    }

    @Override
    public boolean equals(Object other) {

        if (other == null)
            return false;

        if (other == this)
            return true;

        PositionPK tmp = (PositionPK) other;

        return this.rechnung.getRgnr().equals(tmp.rechnung.getRgnr())
              && this.kostenart.getKostenart().equals(tmp.kostenart.getKostenart());
    }
}


