use jpawh;

drop table if exists KundeUUID;

create table KundeUUID (

  id          varchar(36) not null primary key,
  vorname     varchar(40),
  nachname    varchar(40),
  createdAt   timestamp
);
