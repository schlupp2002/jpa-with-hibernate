package de.steve72.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@EntityListeners({KundeListener.class, KundeValidierer.class})
public class Kunde {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 40)
    private String vorname;

    @Column(nullable = false, length = 40)
    private String nachname;

    public Kunde() {

    }

    public Kunde(String vorname, String nachname) {

        this.vorname = vorname;
        this.nachname = nachname;
    }

    // Setter
    public void setNachname(String neu) {

        this.nachname = neu;
    }

    @Override
    public String toString() {

        return String.format("([%d]: %s %s)", this.id, this.vorname, this.nachname);
    }

    public long getId() {
        return id;
    }

    public String getVorname() {
        return vorname;
    }
}
