package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity(name="pojotable")
@Table(
	name="tbl_pojo_table",
	uniqueConstraints=@UniqueConstraint(columnNames={"nachname"})
)
public class PojoTable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String vorname;
	private String nachname;

	/**
	 * Der Standardkonstruktor 
     */
	public PojoTable() {

		this("John", "Dolittle");
	}

	/**
     * Konstruktor mit Parametern
     */
	public PojoTable(String vorname, String nachname) {

		this.vorname = vorname;
		this.nachname = nachname;
	}

	// die Getter-/Setter-Methoden
	
	public long getId() {

		return this.id;
	}	

	public void setId(long id) {
		
		this.id = id;
	}

	
	public String getVorname() {

		return this.vorname;
	}

	public void setVorname(String vorname) {

		this.vorname = vorname;
	}

	
	public String getNachname() {

		return this.nachname;
	}

	public void setNachname(String nachname) {

		this.nachname = nachname;
	}
}
