#Lektion00
##Grundlagen ORM

###Primärschlüssel versus Objektidentität

~~~
  @Entity
  @Id
  @GeneratedValue
  @GeneratedValue(strategy=GenerationType.AUTO)
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  @GeneratedValue(strategy=GenerationType.SEQUENCE)
  @GeneratedValue(strategy=GenerationType.TABLE)

  Bsp.: TABLE
  -----------
  @GeneratedValue(strategy=GenerationType.TABLE, generator="myGenerator")
  @TableGenerator(
    name="myGenerator",
    table="ID_GEN",
    pkColumnName="GEN_KEY",
    pkColumnValue="KUNDE_ID",
    valueColumnName="GEN_VALUE",
    allocationSize=10)
~~~
 
* Sequence-Generatoren
  * AUTO
  * IDENTITY
  * SEQUENCE
  * TABLE
* Table-Generatoren

###POJO-Mapping
