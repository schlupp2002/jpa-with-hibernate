package de.steve72.jpa.model;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class KundeUUID {

  @Id
  private String id;
  
  private String vorname;
  private String nachname;

  public KundeUUID() {

    this.id = UUID.randomUUID().toString();
  }

  public KundeUUID(String vorname, String nachname) {

    this();
    this.vorname = vorname;
    this.nachname = nachname;
  }

  @Override
  public String toString() {

    return String.format("%s: %s, %s", this.id, this.nachname, this.vorname);
  }
}
