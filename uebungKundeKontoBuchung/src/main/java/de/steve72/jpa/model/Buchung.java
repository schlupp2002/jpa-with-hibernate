package de.steve72.jpa.model;

import org.hibernate.annotations.NamedQuery;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;


@Entity
public class Buchung {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long buchungId;

    @ManyToOne
    @JoinColumn(name = "kontoId")
    private Konto konto;

    private BigDecimal value;
    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    //region Konstruktoren
    public Buchung() {
        this.value = BigDecimal.ZERO.setScale(2);
    }

    public Buchung(BigDecimal value, String description) {
        this.value = value;
        this.description=description;
        this.date = new Date();
    }
    //endregion

    //region Getter/Setter
    public Long getBuchungId() {
        return buchungId;
    }

    public Konto getKonto() {
        return konto;
    }

    public void setKonto(Konto konto) {
        this.konto = konto;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
    //endregion
}
