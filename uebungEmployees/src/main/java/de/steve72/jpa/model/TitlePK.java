package de.steve72.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;


@Embeddable
public class TitlePK implements Serializable {

    private Integer emp_no;

    @Column(length = 50)
    private String title;

    @Temporal(TemporalType.DATE)
    private Date from_date;

    //region Konstruktoren
    public TitlePK() {
    }

    public TitlePK(Integer emp_no, String title, Date from_date) {

        this.emp_no = emp_no;
        this.title = title;
        this.from_date = from_date;
    }
    //endregion

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        TitlePK titlePK = (TitlePK) o;

        if (!emp_no.equals(titlePK.emp_no))
            return false;

        if (!title.equals(titlePK.title))
            return false;

        return from_date.equals(titlePK.from_date);

    }

    @Override
    public int hashCode() {

        int result = 5381;

        result *= 37 + emp_no.hashCode();
        result *= 37 + title.hashCode();
        result *= 37 + from_date.hashCode();

        return result;
    }
}
