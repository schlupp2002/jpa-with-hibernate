package de.steve72.jpa.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;


@Entity
public class SparkontoTBC extends AKontoTBC {

    private BigDecimal zinssatz;

    public SparkontoTBC() {
    }

    public SparkontoTBC(BigDecimal zinssatz) {
        super();
        this.zinssatz = zinssatz;
    }

    public SparkontoTBC(String bankname, BigDecimal zinssatz) {

        super(bankname);
        this.zinssatz = zinssatz;
    }
}
