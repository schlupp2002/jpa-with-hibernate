package de.steve72.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "tbl_departments")
public class Department {

    @Id
    @Column(length = 4)
    private String dept_no;

    @Column(length = 40)
    private String dept_name;

    // die N:M-Beziehung mit "Department"
    @OneToMany(mappedBy = "department", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<DepartmentManager> departmentManager;

    //region Konstruktoren
    public Department() {

        this.departmentManager = new HashSet<>();
    }

    public Department(String dept_no, String dept_name) {

        this();
        this.dept_no = dept_no;
        this.dept_name = dept_name;
    }
    //endregion

    //region Getter/Setter
    public String getDept_no() {
        return dept_no;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public Set<DepartmentManager> getDepartmentManager() {
        return departmentManager;
    }

    public void setDepartmentManager(Set<DepartmentManager> departmentManager) {
        this.departmentManager = departmentManager;
    }
    //endregion
}
