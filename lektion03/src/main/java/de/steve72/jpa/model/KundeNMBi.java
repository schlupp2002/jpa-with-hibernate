package de.steve72.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;


@Entity
public class KundeNMBi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String vorname;
    private String nachname;


    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name="Kunde2KontoBi",
            joinColumns = {@JoinColumn(name="Kunde")},
            inverseJoinColumns = {@JoinColumn(name="Konto")}
    )
    private Set<KontoNMBi> konten;

    public KundeNMBi() {

        this.konten = new HashSet<>();
    }

    public KundeNMBi(String vorname, String nachname) {

        this();
        this.vorname = vorname;
        this.nachname = nachname;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append(String.format("[%d] %s, %s %n", id, vorname, nachname));

        for (KontoNMBi k: konten) {

            sb.append(k);
            sb.append("\n");
        }

        return sb.toString();
    }

    public void addKonto(KontoNMBi k) {

        k.addKunde(this);
        this.konten.add(k);
    }

    public Integer getId() {
        return id;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }
}
