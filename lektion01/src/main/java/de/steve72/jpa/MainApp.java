package de.steve72.jpa;

import de.steve72.jpa.model.PojoTable;
import de.steve72.jpa.model.PojoAccess;
import de.steve72.jpa.model.PojoTemporal;
import de.steve72.jpa.model.PojoEnum;
import de.steve72.jpa.model.PojoColumn;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MainApp {

  public static void main(String ... args) {

	// hier entsteht ein "John Doe"
	PojoTable pojotable = new PojoTable();
	PojoAccess pojoaccess = new PojoAccess();
	PojoTemporal pojotemporal = new PojoTemporal();
	PojoEnum pojoenum = new PojoEnum();
	PojoColumn pojocolumn = new PojoColumn();

    // 1. Wir benötigen für unsere persistenten Objekte einen Verwalter
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlPU");
    EntityManager em = emf.createEntityManager();
        
    // 2. der EntityManager soll das Objekt in der Datenbank speichern
    em.getTransaction().begin();

	// em.persist(pojotable);
	em.persist(pojoaccess);
	em.persist(pojotemporal);
	em.persist(pojoenum);
	em.persist(pojocolumn);

    em.getTransaction().commit();
    
    // 3. Aufräumen
    em.clear();
    emf.close();
  }
}
