package de.steve72.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
public class Mitarbeiter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private String vorname;

    @OneToMany(mappedBy = "vorgesetzter", cascade = CascadeType.PERSIST)
    private Set<Mitarbeiter> mitarbeiter;

    @ManyToOne
    @JoinColumn(name="vorgesetzter")
    private Mitarbeiter vorgesetzter;

    public Mitarbeiter() {
    }

    public Mitarbeiter(String name, String vorname) {
        this.name = name;
        this.vorname = vorname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public Mitarbeiter getVorgesetzter() {
        return vorgesetzter;
    }

    public void setVorgesetzter(Mitarbeiter vorgesetzter) {
        this.vorgesetzter = vorgesetzter;
    }
}
