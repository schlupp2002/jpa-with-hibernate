package de.steve72.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;


@Embeddable
public class SalaryPK implements Serializable {

    private Integer emp_no;

    @Temporal(TemporalType.DATE)
    private Date from_date;

    // region Konstruktoren
    public SalaryPK() {
    }

    public SalaryPK(Integer emp_no, Date from_date) {
        this.emp_no = emp_no;
        this.from_date = from_date;
    }
    // endregion

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        SalaryPK salaryPK = (SalaryPK) o;

        if (!emp_no.equals(salaryPK.emp_no))
            return false;

        return from_date.equals(salaryPK.from_date);
    }

    @Override
    public int hashCode() {

        int result = 5381;

        result *= 37 + emp_no.hashCode();
        result *= 37 + from_date.hashCode();

        return result;
    }
}
