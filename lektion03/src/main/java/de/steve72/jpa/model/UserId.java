package de.steve72.jpa.model;

import javax.persistence.Embeddable;
import java.io.Serializable;


@Embeddable
public class UserId implements Serializable {

    private String username;
    private Long departmentId;

    public UserId() {
    }

    public UserId(String username, Long departmentId) {
        this.username = username;
        this.departmentId = departmentId;
    }

    @Override
    public int hashCode() {

        int result = 5381;

        result = 37 * result + username.hashCode();
        result = 37 * result + departmentId.hashCode();

        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if(obj == null)
            return false;

        if(obj == this)
            return true;

        UserId tmp = (UserId) obj;


        return this.departmentId.equals(tmp.departmentId) && this.username.equals(tmp.username);
    }

    // Getter/Setter

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }
}
