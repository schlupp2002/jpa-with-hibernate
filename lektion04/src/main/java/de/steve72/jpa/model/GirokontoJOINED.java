package de.steve72.jpa.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;


@Entity
@DiscriminatorValue("Giro")
public class GirokontoJOINED extends AKontoJOINED{

    private BigDecimal dispo;

    public GirokontoJOINED() {

        super("unknown");
        this.dispo = BigDecimal.ZERO.setScale(2);
    }

    public GirokontoJOINED(String bankname, BigDecimal dispo) {

        super(bankname);
        this.dispo = dispo;
    }
}
