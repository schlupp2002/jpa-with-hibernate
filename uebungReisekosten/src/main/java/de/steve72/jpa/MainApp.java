package de.steve72.jpa;

import de.steve72.jpa.controller.KostenartController;
import de.steve72.jpa.controller.PlzController;
import de.steve72.jpa.model.Kostenart;
import de.steve72.jpa.model.Postleitzahl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;


public class MainApp {

    public static void main(String... args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlPU");
        EntityManager em = emf.createEntityManager();

        // Import der Kostenarten
        em.getTransaction().begin();
        for (Kostenart ka : KostenartController.fromCSV("kostenart.csv")) {

            if (em.find(Kostenart.class, ka.getKostenart()) == null)
                em.persist(ka);
        }
        em.getTransaction().commit();

        // Import der Postleitzahlen
        em.getTransaction().begin();
        Query query = em.createNamedQuery("Postleitzahl.count");
        Long count = (Long) query.getSingleResult();

        if(count.equals(0)) {
            for (Postleitzahl plz : PlzController.fromCSV("postleitzahl.csv")) {

                em.persist(plz);
            }
        }
        em.getTransaction().commit();

        em.clear();
        emf.close();
    }
}
