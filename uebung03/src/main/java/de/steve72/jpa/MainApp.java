package de.steve72.jpa;

import de.steve72.jpa.model.Kostenart;
import de.steve72.jpa.model.Mitarbeiter;
import de.steve72.jpa.model.Position;
import de.steve72.jpa.model.Postleitzahl;
import de.steve72.jpa.model.Rechnung;
import javafx.geometry.Pos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.util.Date;


public class MainApp {

    public static void main(String... args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlPU");
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        // neuer Ort mit PLZ
        Postleitzahl plz = new Postleitzahl("06366", "Köthen/Anhalt");
        em.persist(plz);

        Mitarbeiter ma = new Mitarbeiter("Steffen", "Bauer", "Musterstraße 1");
        ma.setPlz(plz);
        em.persist(ma);

        Kostenart koKm = new Kostenart("km", new BigDecimal(0.25));
        em.persist(koKm);

        Rechnung rg = new Rechnung(new Date(), ma);
        em.persist(rg);

        em.flush();

        Position pos = new Position(rg, koKm, 20);
        em.persist(pos);


        em.getTransaction().commit();

        em.clear();
        emf.close();
    }
}
