package de.steve72.jpa.model;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "tbl_salaries")
public class Salary {

    @EmbeddedId
    private SalaryPK salaryPK;

    @ManyToOne
    @JoinColumn(
          name = "emp_no",
          insertable = false,
          updatable = false)
    private Employee emp_no;

    @Column(nullable = false)
    private Integer salary;

    @Temporal(TemporalType.DATE)
    @Column(nullable = true)
    private Date to_date;

    // region Konstruktoren

    public Salary() {
    }

    public Salary(SalaryPK salaryPK, Integer salary) {
        this.salaryPK = salaryPK;
        this.salary = salary;
    }

    // endregion
}
