package de.steve72.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Kostenart {

    @Id
    @Column(length = 40)
    private String kostenart;

    @Column(precision = 10, scale = 2)
    private BigDecimal einzelkosten;

    // ---------------------------------------------------------------
    // 1-Seite der 1:N-Beziehung zur Assoziationsklasse

    @OneToMany(mappedBy = "kostenart")
    private Set<Position> positionen;

    // Konstruktoren
    public Kostenart() {

        this.positionen = new HashSet<>();
    }

    public Kostenart(String kostenart, BigDecimal einzelkosten) {

        this();
        this.kostenart = kostenart;
        this.einzelkosten = einzelkosten;
    }

    public String getKostenart() {
        return kostenart;
    }

    public void setKostenart(String kostenart) {
        this.kostenart = kostenart;
    }

    public BigDecimal getEinzelkosten() {
        return einzelkosten;
    }

    public void setEinzelkosten(BigDecimal einzelkosten) {
        this.einzelkosten = einzelkosten;
    }
}
