package de.steve72.jpa.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;


@Entity
@Table(name = "tbl_titles")
public class Title {

    @EmbeddedId
    private TitlePK titlePK;

    @ManyToOne
    @JoinColumn(name="emp_no",
          insertable = false,
          updatable = false)
    private Employee emp_no;

    @Temporal(TemporalType.DATE)
    private Date to_date;

    //region Konstruktoren
    public Title() {
    }

    public Title(TitlePK titlePK, Date to_date) {

        this.titlePK = titlePK;
        this.to_date = to_date;
    }
    //endregion

    //region Getter/Setter
    public TitlePK getTitlePK() {
        return titlePK;
    }

    public void setTitlePK(TitlePK titlePK) {
        this.titlePK = titlePK;
    }

    public Employee getEmp_no() {
        return emp_no;
    }

    public void setEmp_no(Employee emp_no) {
        this.emp_no = emp_no;
    }

    public Date getTo_date() {
        return to_date;
    }

    public void setTo_date(Date to_date) {
        this.to_date = to_date;
    }
    //endregion
}
