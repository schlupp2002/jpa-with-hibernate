package de.steve72.jpa;

import de.steve72.jpa.model.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;


public class MainApp {

    public static void main(String... args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlPU");
        EntityManager em = emf.createEntityManager();

        // Test der unidirektionalen 1:1-Beziehung

        Kunde kundeA = new Kunde("Steffen", "Bauer", Date.valueOf(LocalDate.of(1972, Month.APRIL, 1)));
        Adresse adresseA = new Adresse("Musterstraße", "1a", "06366", "Köthen/Anhalt");

        kundeA.setAdresse(adresseA);

        // speichert den Kunden und in einer Cascade die Adresse gleich mit
        em.persist(kundeA);

        em.getTransaction().begin();
        em.getTransaction().commit();

        // den Kunden löschen und in einer Cascade die Adresse gleich mit
        //Kunde opfer = em.find(Kunde.class, 1);
        //em.remove(opfer);

        em.getTransaction().begin();
        em.getTransaction().commit();

        System.out.println(kundeA);

        // ---------------------------------------------------------------------
        // Test der bidirektionalen 1:1 Beziehung

        KundeBi kundeC = new KundeBi("Sebastian", "Kögler");
        AdresseBi adresseB = new AdresseBi("Musterallee", "100", "12345", "Musterdorf");

        kundeC.setAdressebi(adresseB);
        adresseB.setKundebi(kundeC);

        em.persist(kundeC);
        em.persist(adresseB);

        em.getTransaction().begin();
        em.getTransaction().commit();

        System.out.println(kundeC);
        System.out.println(adresseB.show());


        // ---------------------------------------------------------------------
        // Test der unidirektionalen 1:N Beziehung
        Konto konto = new Konto();

        Buchung b1 = new Buchung(new BigDecimal(10000), "Kontoeröffnung");
        Buchung b2 = new Buchung(new BigDecimal(2000), "Panama-Dokumente");
        Buchung b3 = new Buchung(new BigDecimal(-30000), "Passt schon");

        em.persist(konto);
        em.persist(b1);
        em.persist(b2);
        em.persist(b3);

        konto.getBuchungen().add(b1);
        konto.getBuchungen().add(b2);
        konto.getBuchungen().add(b3);

        em.getTransaction().begin();
        em.getTransaction().commit();

        System.out.println(konto);


        // ---------------------------------------------------------------------
        // Test der bidirektionalen 1:N-Beziehung

        KontoBi kontoBi = new KontoBi();

        BuchungBi bi1 = new BuchungBi(new BigDecimal(100), "Bla bla");
        BuchungBi bi2 = new BuchungBi(new BigDecimal(1000), "Bla bla bla");
        BuchungBi bi3 = new BuchungBi(new BigDecimal(10000), "Bla bla bla bla");

        kontoBi.addBuchung(bi1);
        kontoBi.addBuchung(bi2);
        kontoBi.addBuchung(bi3);

        em.persist(kontoBi);

        em.getTransaction().begin();
        em.getTransaction().commit();

        System.out.println(kontoBi);


        // ---------------------------------------------------------------------
        // Test der unidirektionalen N:M-Beziehung

        KundeNM kundeA1 = new KundeNM();
        KundeNM kundeA2 = new KundeNM();

        KontoNM kontoK1 = new KontoNM();
        KontoNM kontoK2 = new KontoNM();
        KontoNM kontoK3 = new KontoNM();
        KontoNM kontoK4 = new KontoNM();


        kundeA1.kundeZuKonto(kontoK1);
        kundeA1.kundeZuKonto(kontoK2);
        kundeA1.kundeZuKonto(kontoK3);

        kundeA2.kundeZuKonto(kontoK4);
        kundeA2.kundeZuKonto(kontoK2);      // Konto k2 gehört Kunden A1 und A2

        em.persist(kundeA1);
        em.persist(kundeA2);

        em.getTransaction().begin();
        em.getTransaction().commit();

        System.out.println(kundeA1);
        System.out.println(kundeA2);


        // ---------------------------------------------------------------------
        // Test der bidirektionalen 1:N Beziehung

        KundeNMBi knmbiA1 = new KundeNMBi("Steffen", "Bauer");
        KundeNMBi knmbiA2 = new KundeNMBi("Tobias", "Aßmann");

        KontoNMBi konmbiK1 = new KontoNMBi();
        KontoNMBi konmbiK2 = new KontoNMBi();
        KontoNMBi konmbiK3 = new KontoNMBi();

        knmbiA1.addKonto(konmbiK1);
        knmbiA1.addKonto(konmbiK2);
        knmbiA1.addKonto(konmbiK3);

        knmbiA2.addKonto(konmbiK1);
        knmbiA2.addKonto(konmbiK3);

        em.persist(knmbiA1);
        em.persist(knmbiA2);

        em.getTransaction().begin();
        em.getTransaction().commit();

        System.out.println(knmbiA1);
        System.out.println(knmbiA2);

        System.out.println(konmbiK1.zeigeKunden());
        System.out.println(konmbiK2.zeigeKunden());
        System.out.println(konmbiK3.zeigeKunden());

        // Test der 1:N-Beziehung mit zusammengesetztem PFK

        em.getTransaction().begin();

        Department department = new Department("Sales");
        em.persist(department);
        em.flush();

        // wir erzeugen uns ein Objekt der Schlüsselklasse
        UserId userId = new UserId("John Doe", department.getId());     // Schlüssel der Abteilung wird gebraucht

        User user = new User(userId);
        em.persist(user);

        em.getTransaction().commit();


        // wir testen die Rekursive Beziehung

        em.getTransaction().begin();

        Mitarbeiter ma1 = new Mitarbeiter("Bauer", "Steffen");
        Mitarbeiter ma2 = new Mitarbeiter("Bauer", "Lars");
        Mitarbeiter ma3 = new Mitarbeiter("Bauer", "Tobias");

        Mitarbeiter chef = new Mitarbeiter("Chefchen", "Absolutes");

        em.persist(ma1);
        em.persist(ma2);
        em.persist(ma3);

        em.persist(chef);

        ma1.setVorgesetzter(chef);
        ma2.setVorgesetzter(chef);
        ma3.setVorgesetzter(chef);

        em.getTransaction().commit();


        em.getTransaction().begin();

        Person p1 = new Person("Susi", "Sorglos");
        em.persist(p1);

        Person p2 = new Person("Goldener", "Fön");
        em.persist(p2);

        p2.heiratet(p1);

        Person p3 = new Person("Prinz", "Auf dem Schimmel");
        em.persist(p3);

        p3.heiratet(p1);

        p2.scheidetSich();

        p3.heiratet(p1);



        em.getTransaction().commit();


        em.clear();
        emf.close();


    }
}
