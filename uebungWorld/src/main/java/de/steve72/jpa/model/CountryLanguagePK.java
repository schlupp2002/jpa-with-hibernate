package de.steve72.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


@Embeddable
public class CountryLanguagePK implements Serializable {

    @Column(length = 3)
    private String countrycode;

    @Column(length = 52)
    private String language;

    public CountryLanguagePK() {
    }

    public CountryLanguagePK(String code, String name) {
        this.countrycode = code;
        this.language = name;
    }

    @Override
    public int hashCode() {

        int result = 5381;

        result *= 37 + countrycode.hashCode();
        result *= 37 + language.hashCode();

        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null)
            return false;

        if (obj == this)
            return true;

        CountryLanguagePK tmp = (CountryLanguagePK) obj;

        return this.countrycode.equals(tmp.countrycode) && this.language.equals(tmp.language);
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String code) {
        this.countrycode = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String name) {
        this.language = name;
    }
}
