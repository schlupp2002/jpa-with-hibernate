package de.steve72.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by bauer on 16.06.16.
 */
@Entity
public class KontoBi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer kontonummer;

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "konto")
    private Set<BuchungBi> buchungen;

    public KontoBi() {

        this.buchungen = new HashSet<>();

    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append("Kontoauszug: ");
        sb.append(this.kontonummer);
        sb.append("\n");

        for(BuchungBi b: buchungen) {

            sb.append(b);
            sb.append("\n");
        }

        return sb.toString();
    }

    public void addBuchung(BuchungBi b) {

        // Bidirektional: Konto muss auch bei der Buchung eingetragen werden
        b.setKonto(this);

        buchungen.add(b);
    }
}
