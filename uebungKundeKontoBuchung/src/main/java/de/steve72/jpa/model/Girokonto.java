package de.steve72.jpa.model;

import javax.persistence.Entity;
import java.math.BigDecimal;


@Entity
public class Girokonto extends Konto {

    private BigDecimal dispo;

    //region Getter/Setter
    public Girokonto() {

        super();
        dispo = BigDecimal.ZERO.setScale(2);
    }

    public Girokonto(String bankname, BigDecimal dispo) {

        super(bankname);
        this.dispo = dispo;
    }
    //endregion

    public BigDecimal getDispo() {
        return dispo;
    }

    public void setDispo(BigDecimal dispo) {
        this.dispo = dispo;
    }
}
