package de.steve72.jpa.model;

import org.hibernate.annotations.GeneratorType;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn
public abstract class Konto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer kontoId;

    @ManyToOne
    @JoinColumn(name = "kundeId")
    protected Kunde kunde;

    @Column(length = 40)
    protected String bankname;

    @OneToMany(mappedBy = "konto", cascade = CascadeType.PERSIST)
    protected Set<Buchung> buchungen;

    @Override
    public String toString() {
        return String.format("%s, %04d %s", this.getClass().getSimpleName(), kontoId, bankname);
    }

    //region Konstruktoren
    public Konto() {
        this.buchungen = new HashSet<>();
    }

    public Konto(String bankname) {
        this();
        this.bankname = bankname;
    }
    //endregion

    //region Getter/Setter
    public Integer getKontoId() {
        return kontoId;
    }

    public Kunde getKunde() {
        return kunde;
    }

    public void setKunde(Kunde kunde) {
        this.kunde = kunde;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public Set<Buchung> getBuchungen() {
        return buchungen;
    }

    //endregion

    // Einzahlungen vornehmen
    public void einzahlen(BigDecimal value, String description) {

        Buchung b = new Buchung(value, description);
        this.buchungen.add(b);
        b.setKonto(this);
    }

    public void auszahlen(BigDecimal value, String description) {

        einzahlen(value.negate(), description);
    }
}