package de.steve72.jpa.model;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name="Art")
public abstract class AKontoJOINED {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer kontonummer;

    protected String bankname;

    public AKontoJOINED() {
    }

    public AKontoJOINED(String bankname) {
        this.bankname = bankname;
    }

    @Override
    public String toString() {
        return String.format("[%s] %3d %s", this.getClass().getSimpleName(), kontonummer, bankname);
    }
}
