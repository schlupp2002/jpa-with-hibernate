package de.steve72.jpa.controller;

import de.steve72.jpa.model.Department;
import de.steve72.jpa.model.DepartmentManager;
import de.steve72.jpa.model.Employee;

/**
 * DepartmentManagerController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class DepartmentManagerController {

    public static DepartmentManager getDepartmentManager(Employee employee, Department dept, String from_date, String to_date) {

        DepartmentManager dptm = new DepartmentManager(employee, dept, DateController.getDateFromString(from_date), DateController.getDateFromString(to_date));

        employee.getDepartmentManager().add(dptm);
        dept.getDepartmentManager().add(dptm);

        return dptm;
    }
}
