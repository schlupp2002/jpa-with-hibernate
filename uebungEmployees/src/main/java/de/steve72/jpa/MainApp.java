package de.steve72.jpa;

import de.steve72.jpa.controller.DepartmentController;
import de.steve72.jpa.controller.DepartmentManagerController;
import de.steve72.jpa.controller.EmployeeController;
import de.steve72.jpa.controller.SalaryController;
import de.steve72.jpa.controller.TitleController;
import de.steve72.jpa.model.Department;
import de.steve72.jpa.model.DepartmentManager;
import de.steve72.jpa.model.Employee;
import de.steve72.jpa.model.Salary;
import de.steve72.jpa.model.Title;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class MainApp {

    public static void main(String... args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlPU");
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        // einen Angestellten erzeugen
        // Employee prakti = new Employee("Tobias", "Aßmann", Employee.GenderType.M, new Date(), new Date());
        Employee prakti = EmployeeController.createEmployee("Tobias", "Aßmann", Employee.GenderType.M, "1977-01-01", "2016-01-04");
        em.persist(prakti);

        // dem erzeugten Angestellten ein Gehalt geben
        Salary salary = SalaryController.getSalary(prakti.getEmp_no(), "2016-01-01", 4000);
        prakti.getSalary().add(salary);

        // dem erzeugten Angestellen einen Title geben
        Title title = TitleController.getTitle(prakti.getEmp_no(), "Excutive Clerk for Heavy Overdosed Functionality", "2016-01-01", "2017-01-01");
        prakti.getTitle().add(title);

        // eine Abteilung anlegen
        Department department = DepartmentController.getDepartment("D001", "Poststelle");
        em.persist(department);

        // einen Abteilungsleiter anlegen
        DepartmentManager departmentManager = DepartmentManagerController.getDepartmentManager(prakti, department, "2016-01-01", "2017-01-01");

        em.flush(); // der DepartmentManager

        System.out.println(prakti.getEmp_no());
        System.out.println(department.getDept_no());
        System.out.println(departmentManager.getDepartment().getDept_no());
        System.out.println(departmentManager.getEmployee().getEmp_no());


        em.getTransaction().commit();

        em.clear();
        emf.close();
    }
}
