package de.steve72.jpa;

import de.steve72.jpa.model.Kunde;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class MainApp {

    public static void main(String... args) {

        // ein noch TRANSIENTES Objekt
        Kunde kunde = new Kunde("Steffen", "Bauer");
        Kunde kundeA = new Kunde("Paulfelz", "Tobi");

        Kunde kundeB = new Kunde("Bbbbb", "Tobi");
        Kunde kundeC = new Kunde("Ccccc", "Tobi");
        Kunde kundeD = new Kunde("Ddddd", "Tobi");
        Kunde kundeE = new Kunde("Eeeee", "Tobi");
        Kunde kundeF = new Kunde("Fffff", "Tobi");

        // 1. Wir benötigen für unsere persistenten Objekte einen Verwalter
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysqlPU");
        EntityManager em = emf.createEntityManager();

        System.out.printf("Im Kontext enthalten %s %b %n", kundeA, em.contains(kundeA));
        em.persist(kundeA);
        System.out.printf("Im Kontext enthalten %s %b %n", kundeA, em.contains(kundeA));

        // 2. der EntityManager soll das Objekt in der Datenbank speichern
        em.getTransaction().begin();

        // wir überführen von TRANSIENT -> VERWALTET
        System.out.printf("Im Kontext enthalten %s %b %n", kunde, em.contains(kunde));
        em.persist(kunde);
        System.out.printf("Im Kontext enthalten %s %b %n", kunde, em.contains(kunde));

        // wir testen das Einfügen der Kunden B-F mit Synchronisation mit der DB von Hand

        em.persist(kundeB);
        em.persist(kundeC);

        em.flush();        // Daten sollen *jetzt* synchronisiert werden

        System.out.println(kundeB);        // die beiden Kunden B und C *müssen* mit ID<>0 ausgegeben werden,
        System.out.println(kundeC);        // weil mit em.flush() manuell gespeichert wurde

        em.persist(kundeD);
        em.persist(kundeE);
        em.persist(kundeF);

        System.out.println(kundeD);        // die drei Kunden D .. E *können* mit ID<>0 ausgeben werden, weil der
        System.out.println(kundeE);        // Entity-Manager zu jedem Zeitpunkt vor dem commit()
        System.out.println(kundeF);        // mit der Datenbank synchronisieren darf

        // hier wird der Persistenzkontext mit der Datenbank synchronisiert
        em.getTransaction().commit();

        // wir laden Entities aus der Datenbank/aus dem Kontext
        Kunde k = em.find(Kunde.class, 2L);        // liefert den Kunden mit der id=2 oder im Fehlerfall null

        System.out.println(k);
        k.setNachname("Kögler");        // der Kunde mit der id=2 wird umbenannt

        em.getTransaction().begin();
        em.getTransaction().commit();

        // -------------------------------------------------------------------------------------------------------
        // wir wollen ein Objekt löschen
        Kunde opfer = em.find(Kunde.class, 3L);
        //em.remove(opfer);

        // alternativ
        //em.remove(em.find(Kunde.class, 5L));

        em.getTransaction().begin();
        em.getTransaction().commit();

        Kunde ausnahme = new Kunde("st", "bauer");

        try {
            em.persist(ausnahme);
        } catch (Exception ex) {

            System.out.println("so nicht!!!");
        }

        // 3. Aufräumen
        em.clear();
        emf.close();
    }
}
