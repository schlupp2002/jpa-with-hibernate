package de.steve72.jpa.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;


@Entity
@DiscriminatorValue("Spar")
public class SparkontoJOINED extends AKontoJOINED {

    private BigDecimal zinssatz;

    public SparkontoJOINED() {
        super("noname");
        this.zinssatz = BigDecimal.ZERO.setScale(2);
    }

    public SparkontoJOINED(String bankname, BigDecimal zinssatz) {
        super(bankname);
        this.zinssatz = zinssatz;
    }

    @Override
    public String toString() {

        return String.format("%s %s", super.toString(), this.zinssatz.toString());
    }
}
