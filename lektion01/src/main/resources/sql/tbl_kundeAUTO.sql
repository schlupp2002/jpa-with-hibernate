use jpawh;

drop table if exists KundeAUTO;

create table KundeAUTO (

  id          bigint unsigned not null auto_increment primary key,
  vorname     varchar(40),
  nachname    varchar(40),
  createdAt   timestamp
);
