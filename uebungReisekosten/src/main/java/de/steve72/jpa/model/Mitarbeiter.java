package de.steve72.jpa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Mitarbeiter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer maid;

    private String vorname;
    private String nachname;
    private String strasse;

    // N-Seite der 1:N-Beziehung PLZ-MA
    @ManyToOne
    @JoinColumn(name="osmid")
    private Postleitzahl osmid;


    // 1:N Beziehung, MA-RG
    @OneToMany(mappedBy ="mitarbeiter")
    private Set<Rechnung> rechnungen;

    // Konstruktoren
    public Mitarbeiter() {

        this.rechnungen = new HashSet<>();
    }

    public Mitarbeiter(String vorname, String nachname, String strasse) {

        this();
        this.vorname = vorname;
        this.nachname = nachname;
        this.strasse = strasse;
    }


    public Integer getMaid() {

        return maid;
    }


    public void setMaid(Integer maid) {

        this.maid = maid;
    }


    public String getVorname() {

        return vorname;
    }


    public void setVorname(String vorname) {

        this.vorname = vorname;
    }


    public String getNachname() {

        return nachname;
    }


    public void setNachname(String nachname) {

        this.nachname = nachname;
    }


    public String getStrasse() {

        return strasse;
    }


    public void setStrasse(String strasse) {

        this.strasse = strasse;
    }


    public Postleitzahl getOsmid() {

        return osmid;
    }


    public void setOsmid(Postleitzahl osmid) {

        this.osmid = osmid;
    }


    public Set<Rechnung> getRechnungen() {

        return rechnungen;
    }


    public void setRechnungen(Set<Rechnung> rechnungen) {

        this.rechnungen = rechnungen;
    }
}
