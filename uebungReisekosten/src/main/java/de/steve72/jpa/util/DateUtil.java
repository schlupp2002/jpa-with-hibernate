package de.steve72.jpa.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * DateUtil
 *
 * @author Steffen Bauer <schlupp2002@gmail.com
 */
public class DateUtil {

    public static Date fromString(String dateString) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;

        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }
}
