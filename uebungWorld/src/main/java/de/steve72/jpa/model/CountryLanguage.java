package de.steve72.jpa.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "tbl_countrylanguage")
public class CountryLanguage {

    @EmbeddedId
    private CountryLanguagePK clpk;

    @Column(columnDefinition = "float(4,1)")
    private Float percentage;

    @ManyToOne
    @JoinColumn(
          name = "countrycode",
          insertable = false,
          updatable = false
    )
    private Country country;

    public CountryLanguage() {
    }

    public CountryLanguage(CountryLanguagePK clpk) {
        this.clpk = clpk;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Float getPercentage() {
        return percentage;
    }

    public void setPercentage(Float percentage) {
        this.percentage = percentage;
    }
}
